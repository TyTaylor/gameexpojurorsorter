﻿Aaron Jones:  players: 1   platforms: windows, steam, iphone, playstation, switch   conflicts: 
Alan Dang:  players: 1   platforms: windows, steam, iphone   conflicts: 
Alexander Bevier:  players: 2   platforms: windows, steam, iphone, ipad, playstation, switch, physical   conflicts: 
Alice Botino:  players: 3   platforms: windows, steam, android, switch, physical   conflicts: 
Alix Harvey:  players: 2   platforms: windows, mac, steam, iphone, xbox, playstation, physical   conflicts: 
Andrea Roberts:  players: 2   platforms: windows, steam, ipad, android, xbox, switch, physical   conflicts: 
Anthony Giovannetti:  players: 1   platforms: windows, steam   conflicts: 
Ashley Rivas:  players: 2   platforms: windows, steam, iphone, xbox, playstation, switch, physical   conflicts: 
August Belhumeur:  players: 2   platforms: windows, mac, steam, iphone, switch, physical   conflicts: 
Bob Roberts:  players: 2   platforms: windows, mac, steam, iphone, ipad, android, xbox, playstation, switch, physical   conflicts: 
Braden League:  players: 1   platforms: windows, steam   conflicts: 
Brandon Dang:  players: 1   platforms: android, switch   conflicts: 
Casey Yano:  players: 1   platforms: windows, mac, steam, switch   conflicts: 
Dana Hanna:  players: 2   platforms: windows, steam, iphone, ipad, xbox, switch, physical   conflicts: 
Daniel Birman:  players: 1   platforms: windows, mac, steam   conflicts: 
Daniel Hepokoski:  players: 1   platforms: windows, steam, android, playstation, switch   conflicts: 
Dave Pavlisak:  players: 2   platforms: windows, steam, ipad, android, switch, physical   conflicts: 
DM Liao:  players: 1   platforms: windows, mac, steam, iphone   conflicts: 
Eric Singleton:  players: 1   platforms: windows, steam, iphone, playstation, switch   conflicts: 
Gordon Lee:  players: 1   platforms: windows, steam, android, xbox, playstation, switch   conflicts: 
Jac Mindelan:  players: 1   platforms: windows, steam, android, xbox, switch   conflicts: 
Jackie Kreitzberg:  players: 2   platforms: windows, steam, android, playstation, switch, physical   conflicts: 
Jaiden Gerig:  players: 2   platforms: windows, linux, steam, android, playstation, switch, physical   conflicts: Towerbolt
Jakub Kasztalski:  players: 1   platforms: windows, steam, android   conflicts: 
Jaycee Salinas:  players: 2   platforms: windows, mac, linux, steam, iphone, ipad, xbox, playstation, switch, vr(quest/vive/etc), physical   conflicts: 
Jon Gill:  players: 2   platforms: windows, mac, steam, iphone, ipad, playstation, switch, physical   conflicts: 
Jorge Gaspar Lira:  players: 2   platforms: windows, mac, steam, iphone, ipad, switch, physical   conflicts: 
Karl Spang:  players: 1   platforms: windows, steam, iphone, xbox   conflicts: 
Kells Tate:  players: 1   platforms: windows, steam, iphone, switch   conflicts: 
Kevin Tarchenski:  players: 2   platforms: windows, mac, steam, iphone, ipad, android, switch, physical   conflicts: 
Kira Premack:  players: 2   platforms: windows, physical, steam   conflicts: 
Malina Karl:  players: 1   platforms: windows, steam, android   conflicts: 
Mateusz Cichenski:  players: 1   platforms: windows, steam, iphone, ipad, android, playstation, switch   conflicts: potworki
Matthew:  players: 1   platforms: windows, steam, android, xbox, switch   conflicts: 
Max Davis:  players: 1   platforms: windows, mac, steam, iphone, switch   conflicts: 
Michael Cooper:  players: 1   platforms: windows, steam, playstation   conflicts: 
Nate Buck:  players: 2   platforms: windows, steam, android, playstation, switch, physical   conflicts: 
Patrick Kemp:  players: 2   platforms: windows, mac, steam, iphone, switch, physical   conflicts: 
Robert Ackley:  players: 1   platforms: windows, steam, android, switch   conflicts: The BridgeMaster
Robert Mock:  players: 2   platforms: windows, linux, steam, android, switch, physical   conflicts: 
Ryan Zhao:  players: 1   platforms: windows, steam, ipad, android, xbox, playstation, switch   conflicts: 
Sebastian Valderrabano:  players: 2   platforms: windows, steam, android, xbox, playstation, switch, physical   conflicts: 
Sela Davis:  players: 2   platforms: windows, steam, iphone, ipad, xbox, playstation, switch, physical   conflicts: 
Shell Juett:  players: 1   platforms: windows, steam, android, playstation, switch   conflicts: Heart of the Dungeon
Stephen Kolodychuk:  players: 1   platforms: windows, steam   conflicts: 
Steven Ngu:  players: 1   platforms: windows, steam, android, playstation, switch   conflicts: Passage: A Job Interview Simulator!
Tania Pavlisak:  players: 2   platforms: windows, steam, ipad, android, switch, physical   conflicts: 
Will Tang:  players: 2   platforms: windows, steam, android, physical   conflicts: 
Zachary Lee:  players: 2   platforms: mac, steam, iphone, switch, physical   conflicts: 
Zack Nickerson:  players: 1   platforms: windows, steam, android, playstation, switch   conflicts: 


Abalon:  players: 1   platforms: windows, mac
Abbot's Gambol:  players: 1   platforms: windows, mac
Après Ski:  players: 1   platforms: windows
Astrolander:  players: 1   platforms: windows
AutoForge:  players: 1   platforms: steam, windows
Azoove:  players: 1   platforms: windows
CENTAURI HEAVY INDUSTRIES:  players: 1   platforms: linux, windows, mac
Crimson Tide:  players: 1   platforms: windows
Crowned:  players: 1   platforms: steam, windows
Crowy Game (WIP):  players: 1   platforms: linux, windows, mac
Dark Fracture:  players: 1   platforms: windows
Desktop Explorer:  players: 1   platforms: windows, mac
Draco and the Seven Sea Scales:  players: 1   platforms: windows
Duality: An Era of Stone:  players: 1   platforms: android
Extreme Evolution: Drive to Divinity:  players: 1   platforms: windows
Fall For You:  players: 1   platforms: linux, windows, mac
Favor: Gods of Oethera:  players: 1   platforms: tabletop/physical
Flush Force:  players: 1   platforms: windows
Fungal Fantasy:  players: 1   platforms: windows
He Who Watches:  players: 1   platforms: steam, windows
Heart of the Dungeon:  players: 1   platforms: windows
High Desert Eclipse:  players: 1   platforms: windows
jaredbruhn@gmail.com:  players: 1   platforms: windows
kCaloria:  players: 1   platforms: tabletop/physical
Kettle Grove: Critter Crafters:  players: 1   platforms: windows
Kitchen Sync: Aloha!:  players: 1   platforms: windows, mac
Law & Judgement: Shadow of the West:  players: 1   platforms: windows, mac
Life After Magic:  players: 1   platforms: windows
Mark My Words:  players: 1   platforms: iphone, android, ipad
Mindfulplay:  players: 1   platforms: iphone, ipad
Minotaur Princess:  players: 1   platforms: steam, windows
Passage: A Job Interview Simulator!:  players: 1   platforms: android, linux, steam, windows
Pippin's Mysterious Garden :  players: 1   platforms: windows, mac
Potions: A Curious Tale:  players: 1   platforms: windows
potworki:  players: 1   platforms: iphone, android, ipad
Presences: Dark Awakening:  players: 1   platforms: windows
Pro Philosopher 2: Governments & Grievances:  players: 1   platforms: windows
Project Anomaly: Urban Supernatural Investigator:  players: 1   platforms: windows
Prospector:  players: 1   platforms: windows
Raia Nova:  players: 1   platforms: 
RAM: Random Access Mayhem:  players: 1   platforms: windows
Rocococo Audiogame Fantastique:  players: 1   platforms: windows, mac
Rogue Labyrinth:  players: 1   platforms: windows
Shift Shaft:  players: 1   platforms: iphone, android, ipad
Spirit & Stone:  players: 1   platforms: steam, windows
Stellar Watch:  players: 1   platforms: windows
Super Cucumber:  players: 1   platforms: windows
TD Royale:  players: 1   platforms: windows
The Art of Flight:  players: 1   platforms: steam, windows
The BridgeMaster:  players: 1   platforms: steam, windows
Throne of Bone:  players: 1   platforms: windows, mac
Tower Song:  players: 1   platforms: windows, mac
Towerbolt:  players: 1   platforms: windows
UpLiftVR 'Maiden Flight' Balloon Ride:  players: 1   platforms: windows
VideoHole: Episode II:  players: 1   platforms: windows
Voyager 2:  players: 1   platforms: iphone, steam, ipad, windows
Wordvoyance:  players: 1   platforms: iphone, android, ipad, windows, mac
