﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GameExpoJurorSorter
{
    public class CsvData
    {
        public string Name;
        public List<string> Headers;
        public List<List<string>> AllData;

        public CsvData(string filePath)
        {
            this.AllData = new List<List<string>>();

            if (File.Exists(filePath))
            {
                FileInfo fileInfo = new FileInfo(filePath);
                this.Name = fileInfo.Name.Replace(fileInfo.Extension, string.Empty);

                CsvConfiguration configuration = new CsvConfiguration();
                Encoding encoding = Encoding.UTF8;
                if (fileInfo.Extension.ToLower().EndsWith("csv"))
                {
                    configuration.Delimiter = ",";
                }
                else
                {
                    encoding = Encoding.Unicode;
                    configuration.Delimiter = "\t";
                }

                using (var stream = new StringReader(File.ReadAllText(filePath, Encoding.UTF8)))
                {
                    using (CsvReader reader = new CsvReader(stream, configuration))
                    {
                        while (reader.Read())
                        {
                            if (Headers == null)
                            {
                                this.Headers = ProcessStringArray(reader.FieldHeaders);
                            }

                            List<string> toAdd = ProcessStringArray(reader.CurrentRecord);
                            if (toAdd != null)
                            {
                                AllData.Add(toAdd);
                            }
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("ERROR: File " + filePath + " does not exist!");
                this.Headers = new List<string>();
                this.Name = string.Empty;
            }
        }

        public int GetHeaderIndex(string key)
        {
            key = key.ToLower().Trim();
            int toReturn = 0;
            foreach (string header in this.Headers)
            {
                if (header.ToLower().Trim().Equals(key))
                {
                    return toReturn;
                }

                ++toReturn;
            }

            return -1;
        }

        private List<string> ProcessStringArray(string[] input)
        {
            bool hasEntry = false;
            List<string> toReturn = new List<string>();
            for (int index = 0; index < input.Length; ++index)
            {
                if (string.IsNullOrEmpty(input[index]))
                {
                    toReturn.Add(string.Empty);
                }
                else
                {
                    hasEntry = true;
                    toReturn.Add(input[index]);
                }
            }

            if (!hasEntry)
            {
                return null;
            }

            return toReturn;
        }
    }
}