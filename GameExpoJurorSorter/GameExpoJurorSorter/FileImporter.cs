﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GameExpoJurorSorter
{
    public static class FileImporter
    {
        public static List<Juror> GetJurors(string filePath)
        {
            CsvData importedData;
            try
            {
                importedData = new CsvData(filePath);
            }
            catch
            {
                WriteError("ERROR importing .csv file " + filePath);
                return null;
            }

            int nameIndex = importedData.GetHeaderIndex("Name");
            int platformsIndex = importedData.GetHeaderIndex("What platforms & consoles are you capable of playing?");
            int peripheralsIndex = importedData.GetHeaderIndex("What peripherals do you own?");
            int multiplayerAvailableIndex = importedData.GetHeaderIndex("Are you available to coordinate and play multiplayer games in-person with your fellow jurors?");
            if (multiplayerAvailableIndex == -1)
            {
                multiplayerAvailableIndex = importedData.GetHeaderIndex("How many people total are quarantined in your home that would also be able to help you jury local multiplayer games?");
            }

            int conflictingGamesIndex = importedData.GetHeaderIndex("Are there any games that you feel you cannot review objectively (e.g. because you're friends with the developers)?");

            List<Juror> toReturn = new List<Juror>();
            foreach (List<string> line in importedData.AllData)
            {
                if (line.Count == 0)
                {
                    WriteError("ERROR: CSV file contains an empty line in " + filePath);
                    return null;
                }

                if (line.Count <= nameIndex)
                {
                    WriteError("ERROR: CSV file contains a line without a name column in " + filePath);
                    return null;
                }

                if (line.Count <= platformsIndex)
                {
                    WriteError("ERROR: CSV file contains a line without the platforms column in " + filePath);
                    return null;
                }

                if (line.Count <= peripheralsIndex)
                {
                    WriteError("ERROR: CSV file contains a line without the peripherals column in " + filePath);
                    return null;
                }

                if (line.Count <= multiplayerAvailableIndex)
                {
                    WriteError("ERROR: CSV file contains a line without the multiplayer availability column in " + filePath);
                    return null;
                }

                if (line.Count <= conflictingGamesIndex)
                {
                    WriteError("ERROR: CSV file contains a line without the conflicting games column in " + filePath);
                    return null;
                }

                string name = line[nameIndex];
                foreach (Juror juror in toReturn)
                {
                    if (Utilities.CompareStrings(name, juror.Name))
                    {
                        WriteError("ERROR: CSV file duplicate names " + name + " and " + juror.Name + " in " + filePath);
                        return null;
                    }
                }

                toReturn.Add(new Juror(name, line[platformsIndex], line[peripheralsIndex], line[multiplayerAvailableIndex], line[conflictingGamesIndex]));
            }

            return toReturn;
        }

        public static List<Game> GetGamesCsv(string filePath)
        {
            CsvData importedData;
            try
            {
                importedData = new CsvData(filePath);
            }
            catch (Exception e)
            {
                WriteError("ERROR importing .csv file " + filePath + "  " + e.Message);
                return null;
            }

            int titleIndex = Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("Game Title"), importedData.GetHeaderIndex("Title"));
            int platformsIndex = Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("Platform(s)"), importedData.GetHeaderIndex("Platforms"));
            int peripheralsIndex = importedData.GetHeaderIndex("Peripherals Required");
            int multiplayerAvailableIndex = Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("How many players does your game support?"), importedData.GetHeaderIndex("Players"));

            List<Game> toReturn = new List<Game>();
            foreach (List<string> line in importedData.AllData)
            {
                if (line.Count == 0)
                {
                    WriteError("ERROR: CSV file contains an empty line in " + filePath);
                    return null;
                }

                if (line.Count <= titleIndex)
                {
                    WriteError("ERROR: CSV file contains a line without a name column in " + filePath);
                    return null;
                }

                if (line.Count <= platformsIndex)
                {
                    WriteError("ERROR: CSV file contains a line without the platforms column in " + filePath);
                    return null;
                }

                if (line.Count <= peripheralsIndex && peripheralsIndex >= 0)
                {
                    WriteError("ERROR: CSV file contains a line without the peripherals column in " + filePath);
                    return null;
                }

                if (line.Count <= multiplayerAvailableIndex && multiplayerAvailableIndex >= 0)
                {
                    WriteError("ERROR: CSV file contains a line without the multiplayer availability column in " + filePath);
                    return null;
                }

                string name = line[titleIndex];
                foreach (Game game in toReturn)
                {
                    if (Utilities.CompareStrings(name, game.Title))
                    {
                        WriteError("ERROR: CSV file duplicate names " + name + " and " + game.Title + " in " + filePath);
                        return null;
                    }
                }

                List<string> peripherals = new List<string>();
				string peripheralsLine = null;
                if (peripheralsIndex >= 0)
				{
					peripheralsLine = line[peripheralsIndex];
                }
                else
                {
                    try
                    {
                        // 2022 change: Each peripheral was its own column
                        if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/peripherals/keyboard"))] == "TRUE")
                        {
                            peripherals.Add("Keyboard");
                        }

                        if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/peripherals/controllers"))] == "TRUE")
                        {
                            peripherals.Add("USB Controllers that work on desktops");
                        }

                        if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/peripherals/mouse"))] == "TRUE")
                        {
                            peripherals.Add("Mouse");
                        }

                        if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/peripherals/oculusVR"))] == "TRUE")
                        {
                            peripherals.Add("Oculus Rift");
                            peripherals.Add("Oculus Quest");
                            peripherals.Add("Oculus Quest I");
                            peripherals.Add("Oculus Quest 2");
                        }

                        if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/peripherals/viveVR"))] == "TRUE")
                        {
                            peripherals.Add("Vive");
                            peripherals.Add("Valve Index");
                        }
                    }
                    catch { }
                }

                string multiplayer = "1";
				if (multiplayerAvailableIndex >= 0)
				{
					multiplayer = line[multiplayerAvailableIndex];
				}

                List<string> platforms = new List<string>();
                string platformsLine = null;
                if (platformsIndex >= 0)
                {
                    platformsLine = line[platformsIndex];
                }
                else
                {
                    // 2022 change: Each platform was its own column
                    if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/platforms/windows"))] == "TRUE")
                    {
                        platforms.Add("PC (Windows)");
                        peripherals.Add("Keyboard");
                        peripherals.Add("Mouse");
                    }

                    if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/platforms/steam"))] == "TRUE")
                    {
                        platforms.Add("Steam (for installing Steam keys)");
                        peripherals.Add("Keyboard");
                        peripherals.Add("Mouse");
                    }

                    if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/platforms/linux"))] == "TRUE")
                    {
                        platforms.Add("Linux");
                        peripherals.Add("Keyboard");
                        peripherals.Add("Mouse");
                    }

                    if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/platforms/mac"))] == "TRUE")
                    {
                        platforms.Add("Mac (OSX)");
                        peripherals.Add("Keyboard");
                        peripherals.Add("Mouse");
                    }

                    if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/platforms/androidPhone"))] == "TRUE")
                    {
                        platforms.Add("Android 7+ (Phone)");
                    }

                    if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/platforms/iPad"))] == "TRUE")
                    {
                        platforms.Add("iPad (iOS 10+)");
                    }

                    if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/platforms/androidTablet"))] == "TRUE")
                    {
                        platforms.Add("Android 7+ (Tablet)");
                    }

                    if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/platforms/iPhone"))] == "TRUE")
                    {
                        platforms.Add("iPhone (iOS 10+)");
                    }

                    if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/platforms/nintendo"))] == "TRUE")
                    {
                        platforms.Add("Nintendo Switch");
                    }

                    if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/platforms/xbox"))] == "TRUE")
                    {
                        platforms.Add("Xbox");
                    }

                    if (line[Utilities.GetFirstNonNegative(importedData.GetHeaderIndex("demo/platforms/playstation"))] == "TRUE")
                    {
                        platforms.Add("PlayStation");
                    }
                }

                if (string.IsNullOrWhiteSpace(peripheralsLine))
                {
                    peripheralsLine = string.Join(",", peripherals);
                }

                if (string.IsNullOrWhiteSpace(platformsLine))
                {
                    platformsLine = string.Join(",", platforms);
                }

                toReturn.Add(new Game(name, platformsLine, peripheralsLine, multiplayer));
            }

            return toReturn;
        }

        public static List<Game> GetGamesJson(string filePath)
        {
            List<Game> toReturn = new List<Game>();

            using (TextReader reader = new StreamReader(filePath))
            {
                var rootObject = JsonConvert.DeserializeObject(reader.ReadToEnd());
                JObject rootList = rootObject as JObject;
                JProperty collections = rootList.First<object>() as JProperty;
                JObject collectionsChild = collections.First<object>() as JObject;
                JProperty gamesList = null;
                foreach (var childObject in collectionsChild.Children())
                {
                    JProperty child = childObject as JProperty;
                    if (child.Name.Equals("games"))
                    {
                        gamesList = child;
                        break;
                    }
                }

                JObject yearsObject = gamesList.First<object>() as JObject;
                JProperty yearsList = yearsObject.First<object>() as JProperty;
                JObject yearObject = yearsList.First<object>() as JObject;
                JProperty yearList = yearObject.First<object>() as JProperty;
                JObject yearsCollectionObject = yearList.First<object>() as JObject;
                JProperty yearsCollectionList = yearsCollectionObject.First<object>() as JProperty;
                JProperty yearsCollectionListToUse = yearsCollectionList;
                while (yearsCollectionListToUse.Next != null)
                {
                    if (yearsCollectionListToUse.Name.Equals(Program.JsonYearToUse))
                    {
                        break;
                    }

                    yearsCollectionListToUse = yearsCollectionListToUse.Next as JProperty;
                }

                JObject gameCollectionObject = yearsCollectionListToUse.First<object>() as JObject;
                foreach (var gameObject in gameCollectionObject.Children())
                {
                    JProperty game = gameObject as JProperty;
                    toReturn.Add(new Game(game.Name, game.First as JObject));
                }
            }

            return toReturn;
        }

        private static void WriteError(string errorMessage)
        {
            Console.Clear();
            Console.WriteLine(errorMessage);
            Console.WriteLine();
        }
    }
}
