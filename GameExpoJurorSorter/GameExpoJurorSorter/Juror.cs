﻿using System;
using System.Collections.Generic;

namespace GameExpoJurorSorter
{
    public class Juror
    {
        public List<string> ConflictingGamesForDebug = new List<string>();

        public Juror(string name, string platformsRaw, string peripheralsRaw, string multiplayerRaw, string conflictingGamesRaw)
        {
            this.Name = name.Trim();
            this.Platforms = Utilities.GetListFromCommaString(platformsRaw);
            this.Peripherals = Utilities.GetListFromCommaString(peripheralsRaw);
            ////this.AbleToPlayMultiplayer = (!string.IsNullOrWhiteSpace(multiplayerRaw) && multiplayerRaw.Trim().Equals("Yes!"));

            switch (multiplayerRaw.Trim())
            {
                case "1 person (I can only jury single player games)":
                    this.Players = 1;
                    break;
                case "2 people":
                    this.Players = 2;
                    break;
                case "3 people":
                    this.Players = 3;
                    break;
                case "4 people":
                    this.Players = 4;
                    break;
                case "More than 4 people":
                    this.Players = 5;
                    break;
                default:
                    throw new Exception("Number of players string for juror not implemented");
            }

            this.ConflictingGamesRaw = conflictingGamesRaw.Trim();
            this.GamesToPlay = new List<Game>();

            if (this.Players > 1)
            {
                this.Platforms.Add("physical");
                this.Peripherals.Add("physical");
            }

            if (this.Platforms.Contains("steam")
                && !this.Platforms.Contains("windows")
                && !this.Platforms.Contains("mac")
                && !this.Platforms.Contains("linux"))
            {
                this.Platforms.Add("windows");
            }

            if ((this.Platforms.Contains("windows")
                || this.Platforms.Contains("mac")
                || this.Platforms.Contains("linux"))
                && !this.Platforms.Contains("steam"))
            {
                this.Platforms.Add("steam");
            }
        }

        public string Name { get; private set; }

        public List<string> Platforms { get; private set; }

        public List<string> Peripherals { get; private set; }

        public int Players { get; private set; }

        public string ConflictingGamesRaw { get; private set; }

        public List<Game> GamesToPlay { get; private set; }

		public override string ToString()
		{
			return this.Name;
		}
	}
}
