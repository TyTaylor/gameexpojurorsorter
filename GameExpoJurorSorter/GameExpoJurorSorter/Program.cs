﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExpoJurorSorter
{
    class Program
    {
        private const int JurorsPerGame = 5;
        private const int MaxGamesPerJuror = 10;

        public static string JsonYearToUse = "2023";
        private static string jurorDataHardcode = @"D:\GameExpoJurorSorter\gameexpojurorsorter\Data\2024\Juror Sign-Up Form 2024 (Responses) - Form Responses 1.csv";
        private static string gameDataHardcode = @"D:\GameExpoJurorSorter\gameexpojurorsorter\Data\2024\2024_games.csv";
        private static string outputDirectory;

        private static List<Juror> jurors;
        private static List<Game> games;

        private static List<KeyValuePair<string, int>> jurorPlatformsAndPeripheralsWithCounts;
        private static List<KeyValuePair<string, int>> gamePlatformsAndPeripheralsWithCounts;

        private static Random random = new Random();

        private static void Main(string[] args)
        {
            Process[] processes = Process.GetProcesses();
            foreach (Process proc in processes)
            {
                if (proc.ProcessName.ToLower().Contains("excel"))
                {
                    proc.Kill();
                }
            }

            Console.WriteLine("Welcome to the juror sorter.");
            Console.WriteLine();
            Console.WriteLine("Please see corresponding example .csv files if this is your first time using this tool.");
            Console.WriteLine();

            GetJurorCsv();
            GetGamesCsv();
            ////GetGamesJson();
            PopulateData();
            ComputeJurorScores();

            jurors.Sort((a, b) => { return a.Name.CompareTo(b.Name); });
            games.Sort((a, b) => { return a.Title.CompareTo(b.Title); });

            PrintResults();
        }

        private static void PrintResults()
        {
            string outputFile = Path.Combine(outputDirectory, "output_games.txt");
            using (TextWriter writer = new StreamWriter(outputFile, false, Encoding.UTF8))
            {
                foreach (Game game in games)
                {
                    writer.Write(game.Title + ": ");

                    bool writeBreak = false;
                    foreach (JurorAndScore jurorAndScore in game.JurorScores)
                    {
                        if (writeBreak)
                        {
                            writer.Write(", ");
                        }

                        writeBreak = true;

                        writer.Write(jurorAndScore.Juror.Name);
                    }

                    writer.WriteLine();
                }
            }

            outputFile = Path.Combine(outputDirectory, "output_jurors.txt");
            using (TextWriter writer = new StreamWriter(outputFile, false, Encoding.UTF8))
            {
                foreach (Juror juror in jurors)
                {
                    writer.Write(juror.Name + ": " + string.Join(", ", juror.GamesToPlay));
                    writer.WriteLine();
                }
            }

            outputFile = Path.Combine(outputDirectory, "debug.txt");
            using (TextWriter writer = new StreamWriter(outputFile, false, Encoding.UTF8))
            {
                foreach (Juror juror in jurors)
                {
                    writer.Write(juror.Name + ":  players: " + juror.Players + "   platforms: " + string.Join(", ", juror.Platforms) + "   conflicts: " + string.Join(", ", juror.ConflictingGamesForDebug));
                    writer.WriteLine();
                }

                writer.WriteLine();
                writer.WriteLine();

                foreach (Game game in games)
                {
                    writer.Write(game.Title + ":  players: " + game.Players + "   platforms: "  + string.Join(", ", game.Platforms));
                    writer.WriteLine();
                }
            }
        }

        private static void ComputeJurorScores()
        {
            foreach (Juror juror in jurors)
            {
                foreach (Game game in games)
                {
                    float score = GetScore(game, juror);
                    if (score > 0)
                    {
                        game.JurorScores.Add(new JurorAndScore(juror, score));
                    }
                }
            }

            foreach (Game game in games)
            {
                game.SortJurorScores();
            }

            games.Sort((a, b) => { return a.JurorScores.Count.CompareTo(b.JurorScores.Count); });

            for (int iteration = 0; iteration < JurorsPerGame; ++iteration)
            {
                // Games are ordered by least to most jurors available
                foreach (Game game in games)
                {
                    int minGameAssignmentCount = 1000;
                    foreach (JurorAndScore jurorAndScore in game.JurorScores)
                    {
                        if (!jurorAndScore.Juror.GamesToPlay.Contains(game))
                        {
                            minGameAssignmentCount = Math.Min(minGameAssignmentCount, jurorAndScore.Juror.GamesToPlay.Count);
                        }
                    }

                    // Jurors are ordered by most to least score for the game.
                    foreach (JurorAndScore jurorAndScore in game.JurorScores)
                    {
                        if (!jurorAndScore.Juror.GamesToPlay.Contains(game))
                        {
                            if (jurorAndScore.Juror.GamesToPlay.Count == minGameAssignmentCount
                                && jurorAndScore.Juror.GamesToPlay.Count < MaxGamesPerJuror)
                            {
                                jurorAndScore.Juror.GamesToPlay.Add(game);
                                break;
                            }
                        }
                    }
                }
            }

            // Cull out jurors from games that they are not assigned to
            foreach (Game game in games)
            {
                for (int index = game.JurorScores.Count - 1; index >= 0; --index)
                {
                    if (!game.JurorScores[index].Juror.GamesToPlay.Contains(game))
                    {
                        game.JurorScores.RemoveAt(index);
                    }
                }
            }
        }

        private static float GetScore(Game game, Juror juror)
        {
            float toReturn = 1;

            if (juror.Players < game.Players)
            {
                // Juror can't play multiplayer
                return 0;
            }

            if (Utilities.GetLetterOnlyLowercase(juror.ConflictingGamesRaw).Contains(Utilities.RemoveArticlesToLower(Utilities.GetLetterOnlyLowercase(game.Title))))
            {
                if (!juror.ConflictingGamesForDebug.Contains(game.Title))
                {
                    juror.ConflictingGamesForDebug.Add(game.Title);
                }

                // Conflict of interest
                return 0;
            }

            bool hasAnyPlatform = false;
            foreach (string gamePlatform in game.Platforms)
            {
                foreach (string jurorPlatform in juror.Platforms)
                {
                    if (Utilities.CompareStrings(gamePlatform, jurorPlatform))
                    {
                        ////// Multiply score by rarity of peripherals and platforms
                        ////toReturn *= (1 + GetKeyIndex(gamePlatformsAndPeripheralsWithCounts, gamePlatform) / (float)gamePlatformsAndPeripheralsWithCounts.Count);
                        ////toReturn *= (1 + GetKeyIndex(jurorPlatformsAndPeripheralsWithCounts, jurorPlatform) / (float)jurorPlatformsAndPeripheralsWithCounts.Count);

                        hasAnyPlatform = true;
                    }
                }
            }

            if (!hasAnyPlatform && game.Platforms.Count > 0)
            {
                // Juror does not have any platform that the game requires
                return 0;
            }

            bool containsVR = false;
            bool hasAnyPeripheral = false;
            foreach (string gamePeripheral in game.Peripherals)
            {
                foreach (string jurorPeripheral in juror.Peripherals)
                {
                    if (Utilities.CompareStrings(gamePeripheral, jurorPeripheral))
                    {
                        ////// Multiply score by rarity of peripherals and platforms
                        ////toReturn *= (1 + GetKeyIndex(gamePlatformsAndPeripheralsWithCounts, gamePeripheral) / (float)gamePlatformsAndPeripheralsWithCounts.Count);
                        ////toReturn *= (1 + GetKeyIndex(jurorPlatformsAndPeripheralsWithCounts, jurorPeripheral) / (float)jurorPlatformsAndPeripheralsWithCounts.Count);

                        hasAnyPeripheral = true;

                        if (jurorPeripheral.ToLower().Contains("oculus")
                            || jurorPeripheral.ToLower().Contains("vive")
                            || jurorPeripheral.ToLower().Contains("gear")
                            || jurorPeripheral.ToLower().Contains("cardboard"))
                        {
                            containsVR = true;
                        }
                    }
                }
            }

            if (!hasAnyPeripheral && game.Peripherals.Count > 0)
            {
                // Juror does not have any peripheral that the game requires
                return 0;
            }

            if (containsVR)
            {
                toReturn *= 2;
            }

            return toReturn;
        }

        private static int GetKeyIndex(List<KeyValuePair<string, int>> list, string value)
        {
            for (int index = 0; index < list.Count; ++index)
            {
                if (list[index].Key.Equals(value))
                {
                    return index;
                }
            }

            throw new Exception("Key " + value + " not found in list.");
        }

        private static void PopulateData()
        {
            Dictionary<string, int> jurorPlatformsAndPeripheralsWithCountsDictionary = new Dictionary<string, int>();
            Dictionary<string, int> gamePlatformsAndPeripheralsWithCountsDictionary = new Dictionary<string, int>();

            foreach (Juror juror in jurors)
            {
                foreach (string platform in juror.Platforms)
                {
                    if (!jurorPlatformsAndPeripheralsWithCountsDictionary.ContainsKey(platform))
                    {
                        jurorPlatformsAndPeripheralsWithCountsDictionary.Add(platform, 0);
                    }

                    jurorPlatformsAndPeripheralsWithCountsDictionary[platform]++;
                }

                foreach (string peripheral in juror.Peripherals)
                {
                    if (!jurorPlatformsAndPeripheralsWithCountsDictionary.ContainsKey(peripheral))
                    {
                        jurorPlatformsAndPeripheralsWithCountsDictionary.Add(peripheral, 0);
                    }

                    jurorPlatformsAndPeripheralsWithCountsDictionary[peripheral]++;
                }
            }

            foreach (Game game in games)
            {
                foreach (string platform in game.Platforms)
                {
                    if (!gamePlatformsAndPeripheralsWithCountsDictionary.ContainsKey(platform))
                    {
                        gamePlatformsAndPeripheralsWithCountsDictionary.Add(platform, 0);
                    }

                    gamePlatformsAndPeripheralsWithCountsDictionary[platform]++;
                }

                foreach (string peripheral in game.Peripherals)
                {
                    if (!gamePlatformsAndPeripheralsWithCountsDictionary.ContainsKey(peripheral))
                    {
                        gamePlatformsAndPeripheralsWithCountsDictionary.Add(peripheral, 0);
                    }

                    gamePlatformsAndPeripheralsWithCountsDictionary[peripheral]++;
                }
            }

            jurorPlatformsAndPeripheralsWithCounts = jurorPlatformsAndPeripheralsWithCountsDictionary.ToList();
            jurorPlatformsAndPeripheralsWithCounts.Sort((pair1, pair2) => pair2.Value.CompareTo(pair1.Value));

            gamePlatformsAndPeripheralsWithCounts = gamePlatformsAndPeripheralsWithCountsDictionary.ToList();
            gamePlatformsAndPeripheralsWithCounts.Sort((pair1, pair2) => pair2.Value.CompareTo(pair1.Value));
        }

        private static void GetJurorCsv()
        {
            Console.WriteLine("Enter the FULL FILE PATH for the juror csv.");
            string potentialPath;

            if (!string.IsNullOrEmpty(jurorDataHardcode) && File.Exists(jurorDataHardcode))
            {
                potentialPath = jurorDataHardcode;
            }
            else
            {
                potentialPath = ReadAndTrim();
            }

            if (!CheckFileExist(potentialPath))
            {
                GetJurorCsv();
                return;
            }

            jurors = FileImporter.GetJurors(potentialPath);
            if (jurors == null)
            {
                GetJurorCsv();
                return;
            }
        }

        private static void GetGamesCsv()
        {
            Console.WriteLine("Enter the FULL FILE PATH for the games csv.");
            string potentialPath;

            if (!string.IsNullOrEmpty(gameDataHardcode) && File.Exists(gameDataHardcode))
            {
                potentialPath = gameDataHardcode;
            }
            else
            {
                potentialPath = ReadAndTrim();
            }

            if (!CheckFileExist(potentialPath))
            {
                GetGamesCsv();
                return;
            }

            outputDirectory = new FileInfo(potentialPath).DirectoryName;

            games = FileImporter.GetGamesCsv(potentialPath);
            if (games == null)
            {
                GetGamesCsv();
                return;
            }

            // Shuffle the games
            for (int i = 0; i < games.Count; ++i)
            {
                int randomIndex = random.Next(games.Count);
                Game temp = games[i];
                games[i] = games[randomIndex];
                games[randomIndex] = temp;
            }
        }

        private static void GetGamesJson()
        {
            Console.WriteLine("Enter the FULL FILE PATH for the games json.");
            string potentialPath;

            if (!string.IsNullOrEmpty(gameDataHardcode) && File.Exists(gameDataHardcode))
            {
                potentialPath = gameDataHardcode;
            }
            else
            {
                potentialPath = ReadAndTrim();
            }

            if (!CheckFileExist(potentialPath))
            {
                GetGamesJson();
                return;
            }

            outputDirectory = new FileInfo(potentialPath).DirectoryName;

            games = FileImporter.GetGamesJson(potentialPath);
            if (games == null)
            {
                GetGamesJson();
                return;
            }

            // Shuffle the games
            for (int i = 0; i < games.Count; ++i)
            {
                int randomIndex = random.Next(games.Count);
                Game temp = games[i];
                games[i] = games[randomIndex];
                games[randomIndex] = temp;
            }
        }

        private static bool CheckFileExist(string potentialPath)
        {
            if (string.IsNullOrEmpty(potentialPath))
            {
                Console.Clear();
                Console.WriteLine("ERROR: You need to enter a full file path.");
                Console.WriteLine();
                Console.WriteLine();
                return false;
            }
            else if (File.Exists(potentialPath))
            {
                return true;
            }
            else
            {
                Console.Clear();
                Console.WriteLine("ERROR: Can't find the file: " + potentialPath);
                Console.WriteLine();
                Console.WriteLine();
                return false;
            }
        }

        private static string ReadAndTrim()
        {
            string toReturn = Console.ReadLine();
            if (!string.IsNullOrEmpty(toReturn))
            {
                toReturn = toReturn.Trim();
            }

            while (toReturn.StartsWith("\""))
            {
                toReturn = toReturn.Substring(1);
            }

            while (toReturn.EndsWith("\""))
            {
                toReturn = toReturn.Substring(0, toReturn.Length - 1);
            }

            return toReturn;
        }
    }
}
