﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExpoJurorSorter
{
    public class JurorAndScore
    {
        public Juror Juror;
        public float Score;

        public JurorAndScore(Juror juror, float score)
        {
            this.Juror = juror;
            this.Score = score;
        }
    }
}
