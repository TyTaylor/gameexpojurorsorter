﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace GameExpoJurorSorter
{
    public class Game
    {
        private static Random random = new Random();

        public Game(string title, string platforms, string peripherals, string mutliplayer)
        {
            this.JurorScores = new List<JurorAndScore>();
            this.Id = Guid.NewGuid().ToString();
            this.Title = title;
            this.Platforms = new List<string>();
            List<string> temp = new List<string>(platforms.Split(new string[] { Environment.NewLine, "\n", "\r", "," }, StringSplitOptions.RemoveEmptyEntries));
            for (int index = 0; index < temp.Count; ++index)
            {
                string toAdd = Utilities.MakePlatformOrPeripheralUniform(temp[index]);
                if (toAdd != null
                    && !this.Platforms.Contains(toAdd))
                {
                    this.Platforms.Add(toAdd);
                }
            }

            this.Peripherals = new List<string>();
            temp = new List<string>(peripherals.Split(new string[] { Environment.NewLine, "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries));
            for (int index = 0; index < temp.Count; ++index)
            {
                string toAdd = Utilities.MakePlatformOrPeripheralUniform(temp[index]);
                if (toAdd != null
                    && !string.IsNullOrWhiteSpace(toAdd)
                    && !this.Peripherals.Contains(toAdd))
                {
                    this.Peripherals.Add(toAdd);
                }
            }

            switch (mutliplayer)
            {
                case "1":
                    this.Players = 1;
                    break;
                case "2":
                    this.Players = 2;
                    break;
                case "3":
                    this.Players = 3;
                    break;
                case "4":
                    this.Players = 4;
                    break;
                case "5+":
                    this.Players = 5;
                    break;
                default:
                    throw new Exception("Number of players string for game not implemented");
            }

            if (this.Platforms.Contains("steam")
                && !this.Platforms.Contains("windows")
                && !this.Platforms.Contains("mac")
                && !this.Platforms.Contains("linux"))
            {
                this.Platforms.Add("windows");
            }
        }

        public Game(string id, JObject properties)
        {
            this.Id = id;
            this.JurorScores = new List<JurorAndScore>();
            this.Peripherals = new List<string>();
            this.Platforms = new List<string>();

            foreach (JProperty property in properties.Children())
            {
                JValue propertyValue = property.Value as JValue;

                switch (property.Name)
                {
                    case "title":
                        this.Title = (propertyValue.Value as string).Trim();
                        break;
                    case "demo":
                        JObject demo = property.First as JObject;
                        foreach (var demoChild in demo.Children())
                        {
                            JProperty demoProperty = demoChild as JProperty;
                            JValue demoValue = demoProperty.Value as JValue;
                            switch (demoProperty.Name)
                            {
                                case "peripherals":
                                    JObject peripheralsObject = demoProperty.First as JObject;
                                    foreach (var peripheralsChild in peripheralsObject.Children())
                                    {
                                        JProperty peripheralProperty = peripheralsChild as JProperty;
                                        JValue peripheralValue = peripheralProperty.Value as JValue;
                                        if ((bool)peripheralValue.Value)
                                        {
                                            string toAdd = Utilities.MakePlatformOrPeripheralUniform(peripheralProperty.Name);
                                            if (!string.IsNullOrEmpty(toAdd))
                                            {
                                                this.Peripherals.Add(toAdd);
                                            }
                                        }
                                    }

                                    break;
                                case "platforms":
                                    JObject platformsObject = demoProperty.First as JObject;
                                    foreach (var platformsChild in platformsObject.Children())
                                    {
                                        JProperty platformsProperty = platformsChild as JProperty;
                                        JValue platformValue = platformsProperty.Value as JValue;
                                        if ((bool)platformValue.Value)
                                        {
                                            string toAdd = Utilities.MakePlatformOrPeripheralUniform(platformsProperty.Name);
                                            if (!string.IsNullOrEmpty(toAdd))
                                            {
                                                this.Platforms.Add(toAdd);
                                            }
                                        }
                                    }

                                    break;
                                case "players":
                                    switch (demoValue.Value as string)
                                    {
                                        case "1":
                                            this.Players = 1;
                                            break;
                                        case "2":
                                            this.Players = 2;
                                            break;
                                        case "3":
                                            this.Players = 3;
                                            break;
                                        case "4":
                                            this.Players = 4;
                                            break;
                                        case "5+":
                                            this.Players = 5;
                                            break;
                                        default:
                                            throw new Exception("Number of players string for game not implemented");
                                    }

                                    break;
                            }
                        }

                        break;
                }
            }
        }

        public string Id { get; private set; }

        public string Title { get; private set; }

        public List<string> Platforms { get; private set; }

        public List<string> Peripherals { get; private set; }

        public int Players { get; private set; }

        public List<JurorAndScore> JurorScores { get; private set; }

        public void SortJurorScores()
        {
            // Shuffle, then sort (so that any tieing scores are random)
            for (int index = 0; index < this.JurorScores.Count; ++index)
            {
                int randomIndex = random.Next(this.JurorScores.Count);
                JurorAndScore temp = this.JurorScores[index];
                this.JurorScores[index] = this.JurorScores[randomIndex];
                this.JurorScores[randomIndex] = temp;
            }

            this.JurorScores.Sort((a, b) => { return b.Score.CompareTo(a.Score); });
        }

		public override string ToString()
		{
			return this.Title;
		}
	}
}
