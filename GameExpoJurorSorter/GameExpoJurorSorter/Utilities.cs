﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExpoJurorSorter
{
    public static class Utilities
    {
        public static bool CompareStrings(string a, string b)
        {
            if (a == null || b == null)
            {
                return false;
            }

            return a.Trim().ToLower().Replace(" ", string.Empty).Contains(b.Trim().ToLower().Replace(" ", string.Empty))
                || b.Trim().ToLower().Replace(" ", string.Empty).Contains(a.Trim().ToLower().Replace(" ", string.Empty));
        }

		public static int GetFirstNonNegative(params int[] values)
		{
			foreach (int value in values)
			{
				if (value >= 0)
				{
					return value;
				}
			}

			return -1;
		}

        public static string RemoveArticlesToLower(string value)
        {
            return value.ToLower().Replace("the", "").Replace("of", "").Replace("and", "").Replace("or", "").Replace("it", "");
        }

        public static string GetLetterOnlyLowercase(string value)
        {
            string toReturn = string.Empty;
            foreach (char c in value)
            {
                if (char.IsLetter(c))
                {
                    toReturn += c;
                }
            }

            return toReturn.ToLower();
        }

        public static List<string> GetListFromCommaString(string raw)
        {
            List<string> toReturn = new List<string>();
            if (string.IsNullOrWhiteSpace(raw))
            {
                return toReturn;
            }

            string[] split = raw.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string value in split)
            {
                string trimmed = value.Trim();
                if (!string.IsNullOrWhiteSpace(trimmed))
                {
                    string toAdd = MakePlatformOrPeripheralUniform(trimmed);
                    if (!string.IsNullOrEmpty(toAdd)
                        && !toReturn.Contains(toAdd))
                    {
                        toReturn.Add(toAdd);
                    }
                }
            }

            return toReturn;
        }

        public static string MakePlatformOrPeripheralUniform(string input)
        {
            string toReturn = input.ToLower().Replace(" ", "");

            if (toReturn.Contains("controller"))
            {
                toReturn = "controllers";
            }

            if (toReturn.Contains("android") && toReturn.Contains("tablet"))
            {
                toReturn = "androidtablet";
            }

            if (toReturn.Contains("android") && toReturn.Contains("phone"))
            {
                toReturn = "androidphone";
			}

			if (toReturn.Contains("android"))
			{
				toReturn = "android";
			}

			if (toReturn.Contains("ipad"))
            {
                toReturn = "ipad";
            }

            if (toReturn.Contains("iphone"))
            {
                toReturn = "iphone";
            }

            if (toReturn.Contains("mac"))
            {
                toReturn = "mac";
            }

            if (toReturn.Contains("steam"))
            {
                toReturn = "steam";
            }

            if (toReturn.Contains("pc(windows)"))
            {
                toReturn = "windows";
            }

			if (toReturn.Contains("controllers"))
            {
                toReturn = "controllers";
            }

            if (toReturn.Contains("vivevr"))
            {
                toReturn = "vive";
            }

			if (toReturn.Contains("pc"))
			{
				toReturn = "windows";
			}

            if (toReturn.Contains("nintendoswitch"))
            {
                toReturn = "switch";
            }

            if (toReturn.Contains("xboxone"))
            {
                toReturn = "xbox";
            }

            if (toReturn.Contains("playstation4") || toReturn.Contains("playstation5"))
            {
                toReturn = "playstation";
            }

			toReturn = toReturn.Replace(" ", string.Empty);

            if (toReturn.Equals("other")
                || toReturn.Equals("touch"))
            {
                return null;
            }

            return toReturn;
        }
    }
}
